<?php
/** 
 * Main Warden Manager PHP script
 * @author Jorgeley <jinacio@theemaillaundry.com>
 */
use warden\WardenPool;
use warden\DAOFactory;
use warden\WardenManager;
use warden\WardenFileSystem;

require __DIR__ . '/vendor/autoload.php';

echo "\n *** The Email Laundry Warden Manager ***\n";

if (isset($_SERVER['HTTP_USER_AGENT'])){
    echo "<p>Please, execute this script from the command line, like:<br>"
                        . "<i>php warden.php</i></p>";
    exit;
}else{
    //find wardens updated 1 hour ago
    $time = '01:00:00';
    WardenFileSystem::log("Starting warden parser...");  
    WardenFileSystem::log("Checking for new ideas files every $time...");
    
    //instantiate the WardenManager responsible for manipulating the wardens
    $wardenManager = WardenManager::getWardenManager();
    
    //get all DAOs instances for all databases specified in 'database.ini' file
    $daos = DAOFactory::getDAO(DAOFactory::ALL);
    //print_r($daos);
    
    //create a Pool of DAO's objects
    $pool = WardenPool::getPool();

    try{
        WardenFileSystem::log($pool->count()." objects in memory...");
        foreach ($daos as $dao){
             //find the latest idea events
             $ideasUpdated = NULL;
             $ideasUpdated = $dao->findByDetectTime($time);
             $wardenManager->export($ideasUpdated);
             foreach ($ideasUpdated as $key=>$value) unset($ideasUpdated[$key]);
        }
    }catch(Exception $e){
        WardenFileSystem::log('ERROR: '
                .$e->getMessage()."\n"
                .$e->getTraceAsString()."\n"
                .$e->getFile()."\n"
                .$e->getCode()."\n"
                .$e->getLine());
    }
    WardenPool::getPool()->disposeAll();
    WardenFileSystem::log('Warden Parser finished, exported '.$wardenManager->getExported().' events.');
}
