<?php
/**
 * Class responsible for manipulating Warden objects: importing, exporting, etc
 *
 * @category FileSystem
 * @package  FileSystem
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
namespace warden\src;
require_once __DIR__.'/WardenPool.php';
require_once __DIR__.'/Warden.php';
require_once __DIR__.'/WardenFileSystem.php';
use warden\src\WardenPool;
use warden\src\WardenFileSystem;
use DeepCopy\Exception\CloneException;
/**
 * Class responsible for manipulating Warden objects: importing, exporting, etc
 *
 * @category FileSystem
 * @package  FileSystem
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @access   public
 * @uses     \warden\src\WardenPool The pool of wardens objects
 * @uses     \warden\src\WardenFileSystem The class responsible for importing/exporting from/to file system
 * @uses     \DeepCopy\Exception\CloneException Exception when trying to clone the object
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
class WardenManager extends WardenFileSystem
{
    
    /**
     * \warden\src\WardenManager The WardenManager instance
     *
     * @static
     */
    private static $_wardenManager;

    /**
     * Constant Warden version
     */
    const WARDEN_VERSION = '';
    
    /**
     * Array Array of Warden objects
     */
    private $_wardens = [];
    
    /**
     * \warden\src\WardenPool The WardenPool instance
     *
     * @static
     */
    private static $_wardenPool;
    
    /**
     * No one can access the constructor
     */
    private function __construct()
    {        
    }
    
    /**
     * Not even clone
     *
     * @throws CloneException
     * @return void
     */
    public function __clone()
    {
        throw new CloneException();
    }
    
    /**
     * Get an instance of WardenManager
     *
     * @static
     * @return \warden\src\WardenManager
     */
    public static function getWardenManager()
    {
        if (self::$_wardenManager == null) {
            self::$_wardenManager = new WardenManager;
            self::$_wardenPool = WardenPool::getPool();
        }
        return self::$_wardenManager;
    }
    
    /**
     * Instantiate Warden objects for each Json idea from $ideasArray and
     * store them into self::wardens array
     *
     * @param array $ideasArray An array of Json idea contents and name as key, eg:
     *                          ['filename1'] => [
     *                          ["id"]  => "123abc",
     *                          ["name"]=> "idea name",
     *                          ...
     *                          ],
     *                          ['filename2] => ...
     * 
     * @return void
     */
    public function import($ideasArray)
    {
        WardenFileSystem::log("importing idea objects...");
        if (is_array($ideasArray)) {
            foreach ($ideasArray as $fileName=>$ideaArray) {
                try{
                    $warden = self::$_wardenPool->get();
                    $warden->setId($ideaArray['ID']);
                    $warden->setName($ideaArray['Attach'][0]['Filename'][0]);
                    $warden->setHash($ideaArray['Attach'][0]['Hash'][0]);
                    $warden->setHex($ideaArray['Attach'][0]['Content']);
                    $warden->setFormat($ideaArray['Format']);
                    $warden->setDetectTime($ideaArray['DetectTime']);
                    $warden->setCategory($ideaArray['Category']);
                    $warden->setActive($ideaArray['Confidence']);
                    $warden->setCount($ideaArray['_count']);
                    $this->_wardens[$fileName] = $warden;
                    self::$_wardenPool->dispose($warden);
                } catch (Exception $e){
                    WardenFileSystem::log("Error trying to set warden from filesystem!");
                }
            }
        }
        //print_r($this->wardens);
    }
    
    /**
     * Gets all warden ID's
     * 
     * @return array An array of wardens Id's
     */
    public function getWardensIDs()
    {
        $wardensIDs = [];
        foreach ($this->_wardens as $warden) {
            $wardensIDs[] = $warden->getId();
            self::$_wardenPool->dispose($warden);
        }
        return $wardensIDs;
    }
    
    /**
     * Gets all warden objects
     * 
     * @return array Array of \warden\src\Warden objects
     */
    public function getWardens()
    {
        return $this->_wardens;
    }
    
    /**
     * Sets warden objects
     * 
     * @param array $wardens Array of \warden\src\Warden objects
     * 
     * @return void
     */
    public function setWardens($wardens)
    {
        $this->_wardens = $wardens;
    }
    
    /**
     * Unset all warden objects
     * 
     * @return void
     */
    public function unsetWardens()
    {
        foreach ($this->_wardens as $key=>$value) {
            unset($this->_wardens[$key]);
        }
    }
}
