USE av_signatures
TRUNCATE custom_signatures;
INSERT INTO custom_signatures VALUES(null, 'sample mail id', 'sample file name 1', 100, 1, 1, last_insert_id()+1, 'hex sample 1', 1, NOW());
TRUNCATE url_signatures;
INSERT INTO url_signatures VALUES(null, 1, 'sample source id 1', 'sample url 1', '192.168.1.1', 'hex sample 1', last_insert_id()+1, 1, 'brand sample 1', 'groups sample 1', 'UK', 'sample tld 1', 'sample asn 1', 'sample asn name 1', 1, 1, 1, 'sample notes 1', NOW(), NOW());

USE hashdb;
TRUNCATE emailhashes;
INSERT INTO emailhashes VALUES(null, 'sample hash 1', 'sample email 1', 1, NOW());
TRUNCATE hashes;
INSERT INTO hashes VALUES(null, 'sample hash 1', 1, NOW());

USE spamdb;
TRUNCATE hashes;
INSERT INTO hashes VALUES(null, 'sample hash 1', 1, NOW());
