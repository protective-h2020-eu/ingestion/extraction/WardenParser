-- MySQL dump 10.13  Distrib 5.7.20, for Linux (i686)
--
-- Host: localhost    Database: av_signatures
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP DATABASE IF EXISTS av_signatures;
CREATE DATABASE av_signatures;
USE av_signatures;

--
-- Table structure for table `custom_signatures`
--

DROP TABLE IF EXISTS `custom_signatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_signatures` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mail_id` varbinary(16) NOT NULL,
  `file_name` varchar(255) DEFAULT '',
  `size` int(10) unsigned NOT NULL,
  `type` int(2) NOT NULL DEFAULT '1',
  `count` int(10) NOT NULL DEFAULT '1',
  `md5` char(32) NOT NULL,
  `hex` varbinary(4096) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `md5` (`md5`)
) ENGINE=InnoDB AUTO_INCREMENT=56963 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `url_signatures`
--

DROP TABLE IF EXISTS `url_signatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `url_signatures` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_id` int(10) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `url` varchar(2048) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `hex` varbinary(4096) NOT NULL,
  `md5` char(32) NOT NULL,
  `confidence` int(10) NOT NULL DEFAULT '100',
  `brand` varchar(255) DEFAULT NULL,
  `groups` varchar(255) DEFAULT NULL,
  `country_code` varchar(2) DEFAULT NULL,
  `tld` varchar(255) DEFAULT NULL,
  `asn` varchar(16) DEFAULT NULL,
  `asn_name` varchar(255) DEFAULT NULL,
  `type` int(2) NOT NULL DEFAULT '3',
  `count` int(10) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `notes` varchar(255) DEFAULT NULL,
  `discover_time` varchar(255) DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `md5` (`md5`)
) ENGINE=InnoDB AUTO_INCREMENT=22305 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

INSERT INTO custom_signatures VALUES(null, 'sample mail id 1', 'sample file name 1', 100, 1, 1, 'md5 sample 1', 'hex sample 1', 1, '2017-11-16 10:30:00');

INSERT INTO url_signatures VALUES(null, 1, 'sample source id 1', 'sample url 1', '192.168.1.1', 'hex sample 1', 'md5 sample 1', 1, 'brand sample 1', 'groups sample 1', 'UK', 'sample tld 1', 'sample asn 1', 'sample asn name 1', 1, 1, 1, 'sample notes 1', '2017-11-16 10:30:00', '2017-11-16 10:30:00');

-- Dump completed on 2017-11-16 10:17:23
