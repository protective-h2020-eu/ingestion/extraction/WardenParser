<?php
/** 
 * Main Warden Manager PHP script
 * @author Jorgeley <jinacio@theemaillaundry.com>
 */
require_once __DIR__.'/src/DAOFactory.php';
require_once __DIR__.'/src/WardenManager.php';
require_once __DIR__.'/src/WardenPool.php';
use warden\src\WardenPool;
use warden\src\DAOFactory;
use warden\src\WardenManager;

echo "\n *** The Email Laundry Warden Manager ***\n";

if (isset($_SERVER['HTTP_USER_AGENT'])){
    echo "<p>Please, execute this script from the command line, like:<br>"
                        . "<i>php warden.php</i></p>";
    exit;
}else{
    file_put_contents('warden.log', '['.date('d/m/y H:i:s').'] Starting warden parser...', FILE_APPEND);  
    //instantiate the WardenManager responsible for manipulating the wardens
    $wardenManager = WardenManager::getWardenManager();
    
    try{
       //find the wardens in database 'av_signatures', table 'custom_signatures'
       $ideasAlreadyExported = $wardenManager->getIdeasFromFileSystem();
       $wardenManager->import($ideasAlreadyExported);
       foreach ($ideasAlreadyExported as $key=>$value) unset($ideasAlreadyExported[$key]);
       $ideasIds = $wardenManager->getWardensIDs();
       $wardenManager->unsetWardens(); //free some memory
       //get Mysql DAO instance for 'av_signatures' database, table 'custom_signatures'
       $avCustomSignaturesMysqlDAO = DAOFactory::getDAO( DAOFactory::MYSQL_AVCUSTOMSIGNATURES );
       $ideasUnique = $avCustomSignaturesMysqlDAO->findByUniqueWardens($ideasIds);
       foreach ($ideasIds as $key=>$value) unset($ideasIds[$key]);
       $wardenManager->export($ideasUnique);
       foreach ($ideasUnique as $key=>$value) unset($ideasUnique[$key]);
    
       //find the wardens in database 'av_signatures', table 'url_signatures'
       $ideasAlreadyExported = $wardenManager->getIdeasFromFileSystem();
       $wardenManager->import($ideasAlreadyExported);
       foreach ($ideasAlreadyExported as $key=>$value) unset($ideasAlreadyExported[$key]);
       $ideasIds = $wardenManager->getWardensIDs();
       $wardenManager->unsetWardens(); //free some memory
       //get Mysql DAO instance for 'av_signatures' database, table 'url_signatures'
       $avUrlSignaturesMysqlDAO = DAOFactory::getDAO( DAOFactory::MYSQL_AVURLSIGNATURES );
       $ideasUnique = $avUrlSignaturesMysqlDAO->findByUniqueWardens($ideasIds);
       foreach ($ideasIds as $key=>$value) unset($ideasIds[$key]);
       $wardenManager->export($ideasUnique);
       foreach ($ideasUnique as $key=>$value) unset($ideasUnique[$key]);

       //find the wardens in database 'hashdb', table 'emailhashes'
       $ideasAlreadyExported = $wardenManager->getIdeasFromFileSystem();
       $wardenManager->import($ideasAlreadyExported);
       foreach ($ideasAlreadyExported as $key=>$value) unset($ideasAlreadyExported[$key]);
       $ideasIds = $wardenManager->getWardensIDs();
       $wardenManager->unsetWardens(); //free some memory
       //get Mysql DAO instance for 'hashdb' database, table 'emailhashes'
       $emailHashesMysqlDAO = DAOFactory::getDAO( DAOFactory::MYSQL_EMAILHASHES );
       $ideasUnique = $emailHashesMysqlDAO->findByUniqueWardens($ideasIds);
       foreach ($ideasIds as $key=>$value) unset($ideasIds[$key]);
       $wardenManager->export($ideasUnique);
       foreach ($ideasUnique as $key=>$value) unset($ideasUnique[$key]);
    
       /**
        * NOT PARSING THIS DATA SOURCES FOR NOW
       //find the wardens in database 'hashdb', table 'hashes'
       $ideasAlreadyExported = $wardenManager->getIdeasFromFileSystem();
       $wardenManager->import($ideasAlreadyExported);
       foreach ($ideasAlreadyExported as $key=>$value) unset($ideasAlreadyExported[$key]);
       $ideasIds = $wardenManager->getWardensIDs();
       $wardenManager->unsetWardens(); //free some memory
       //get Mysql DAO instance for 'hashdb' database, table 'hashes'
       $hashesMysqlDAO = DAOFactory::getDAO( DAOFactory::MYSQL_HASHES );
       $ideasUnique = $hashesMysqlDAO->findByUniqueWardens($ideasIds);
       foreach ($ideasIds as $key=>$value) unset($ideasIds[$key]);
       $wardenManager->export($ideasUnique);
       foreach ($ideasUnique as $key=>$value) unset($ideasUnique[$key]);
    
       //find the wardens in database 'spamdb', table 'hashes'
       $ideasAlreadyExported = $wardenManager->getIdeasFromFileSystem();
       $wardenManager->import($ideasAlreadyExported);
       foreach ($ideasAlreadyExported as $key=>$value) unset($ideasAlreadyExported[$key]);
       $ideasIds = $wardenManager->getWardensIDs();
       $wardenManager->unsetWardens(); //free some memory
       //get Mysql DAO instance for 'spamdb' database, table 'hashes'
       $spamHashesMysqlDAO = DAOFactory::getDAO( DAOFactory::MYSQL_SPAMHASHES );
       $ideasUnique = $spamHashesMysqlDAO->findByUniqueWardens($ideasIds);
       foreach ($ideasIds as $key=>$value) unset($ideasIds[$key]);
       $wardenManager->export($ideasUnique);
       foreach ($ideasUnique as $key=>$value) unset($ideasUnique[$key]);
        */
   }catch(Exception $e){
       echo $e->getMessage();
       file_put_contents('warden.log', '['.date('d/m/y H:i:s').'] ERROR: '.$e->getMessage(), FILE_APPEND);
   }
   
    //create a Pool of DAO's objects
    $pool = WardenPool::getPool();
    //free some memory
    $pool->disposeAll();

    /**
     * find wardens updated 1 minutes ago
     * @todo check performance of this loop, make a log
     */
    $time = '00:01:00';
    echo " ".$pool->count()." objects in memory...\n";
    echo " Checking for new ideas files every $time...\n";
    while (true){
        try{
           //find the latest database 'av_signatures', table 'custom_signatures'
           $ideasUpdated = $avCustomSignaturesMysqlDAO->findByDetectTime($time);
           $wardenManager->export($ideasUpdated);
           foreach ($ideasUpdated as $key=>$value) unset($ideasUpdated[$key]);
        
           //find the latest database 'av_signatures', table 'url_signatures'
           $ideasUpdated = $avUrlSignaturesMysqlDAO->findByDetectTime($time);
           $wardenManager->export($ideasUpdated);
           foreach ($ideasUpdated as $key=>$value) unset($ideasUpdated[$key]);
        
           //find the latest database 'hashdb', table 'emailhashes'
           $ideasUpdated = $emailHashesMysqlDAO->findByDetectTime($time);
           $wardenManager->export($ideasUpdated);
           foreach ($ideasUpdated as $key=>$value) unset($ideasUpdated[$key]);

           /**
           //find the latest database 'hashdb', table 'hashes'
           $ideasUpdated = $hashesMysqlDAO->findByDetectTime($time);
           $wardenManager->export($ideasUpdated);
           foreach ($ideasUpdated as $key=>$value) unset($ideasUpdated[$key]);

           //find the latest database 'spamdb', table 'hashes'
           $ideasUpdated = $spamHashesMysqlDAO->findByDetectTime($time);
           $wardenManager->export($ideasUpdated);
           foreach ($ideasUpdated as $key=>$value) unset($ideasUpdated[$key]);
            */
       }catch(Exception $e){
           echo $e->getMessage();
           file_put_contents('warden.log', '['.date('d/m/y H:i:s').'] ERROR: '.$e->getMessage(), FILE_APPEND);
       }
       sleep(strtotime("01/01/1970 $time") + 3600);
    }
}