<?php
/**
 * Interface for DAO's
 *
 * @category DAO
 * @package  DAO
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
namespace warden;
/**
 * Interface for DAO's
 *
 * @category DAO
 * @package  DAO
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @access   public
 * @uses     \warden\src\DAO The interface for DAO's
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
interface DAO
{

    /**
     * It might garantee that find unique Idea objects from the Data Source
     *
     * @param array $wardensIds The wardens ID already found and exported to the filesystem
     * 
     * @return array An array of Warden unique objects
     */
    public function findByUniqueWardens($wardensIds);
    
    /**
     * Find new Idea files since last execution specified by $time
     *
     * @param string $time 00:30:00 (it fetch new ideas from 30 minutes ago)
     * 
     * @return array An array of Warden unique objects
     */
    public function findByDetectTime($time);
    
    /**
     * It might garantee that the connection is ok
     *
     * @return boolean The connection status
     */
    public function isConnected();

}