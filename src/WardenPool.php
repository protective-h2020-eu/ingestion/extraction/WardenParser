<?php
/**
 * Pool of \warden\src\Warden objects
 *
 * @category Pool
 * @package  Pool
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
namespace warden;
use warden\Warden;
use warden\WardenFileSystem;
/**
 * Pool of \warden\src\Warden objects
 *
 * @category Pool
 * @package  Pool
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @access   public
 * @uses     \warden\src\Warden The Warden bean
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
class WardenPool implements \Countable
{
    
    /**
     * \warden\src\WardenPool the WardenPool instance
     *
     * @static
     */
    private static $_wardenPool;
    
    /**
     * Array The busy \warden\src\Warden objects
     *
     * @static
     */
    private static $_busyWardens = [];
    
    /**
     * Array The free \warden\src\Warden objects
     *
     * @static
     */
    private static $_freeWardens = [];
    
    /**
     * No one can access the constructor
     */
    private function __construct()
    {        
    }
    
    /**
     * Not even clone
     *
     * @throws CloneException
     * @return void
     */
    public function __clone()
    {
        throw new CloneException();
    }
    
    /**
     * Gets the pool object
     * 
     * @static
     * @return \warden\src\WardenPool The pool object
     */
    public static function getPool()
    {
        if (self::$_wardenPool == null ) {
            self::$_wardenPool = new WardenPool;
        }
        return self::$_wardenPool;
    }

    /**
     * Gets a recycled Warden object from the pool or a new one
     * 
     * @static
     * @return \warden\src\Warden The recycled/new Warden object
     */
    public static function get()
    {
        if (count(self::$_freeWardens) == 0) {
            $warden = new Warden;
        } else {
            $warden = array_pop(self::$_freeWardens);
        }
        self::$_busyWardens[spl_object_hash($warden)] = $warden;
        return $warden;
    }
    
    /**
     * Sets a busy Warden object into the pool
     * 
     * @param \warden\src\Warde $warden Warden object to put into the pool
     * 
     * @return void
     */
    public function set($warden)
    {
        self::$_busyWardens[spl_object_hash($warden)] = $warden;
    }
    
    /**
     * Dispose a \warden\src\Warden object
     *
     * @param \warden\src\Warden $warden The Warden object
     * 
     * @static
     * 
     * @return void
     */
    public static function dispose($warden)
    {
        $key = spl_object_hash($warden);
        if (isset(self::$_busyWardens[$key])) {
            WardenFileSystem::log("Pool: disposing $key...");
            unset(self::$_busyWardens[$key]);
            self::$_freeWardens[$key] = $warden;
        }
    }
    
    /**
     * Unset the whole pool
     *
     * @static
     * 
     * @return void
     */
    public static function disposeAll()
    {
        WardenFileSystem::log("Pool: disposing all...");
        foreach (self::$_busyWardens as $warden){
            unset($warden);
        }
        foreach (self::$_freeWardens as $warden){
            unset($warden);
        }
        self::$_busyWardens = array();
        self::$_freeWardens = array();
    }

    /**
     * Counts the objects within the pool
     * 
     * @return int The \warden\src\Warden total objects in the pool
     */
    public function count() 
    {
        return count(self::$_busyWardens) + count(self::$_freeWardens);
    }

}