<?php
/**
 * Class responsible for manipulating JSON idea files
 * 
 * @category FileSystem
 * @package  FileSystem
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
namespace warden;
use warden\WardenPool;
use Exception;
/**
 * Class responsible for manipulating JSON idea files
 *
 * @abstract
 * @category FileSystem
 * @package  FileSystem
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @access   public
 * @uses     \warden\src\DAO The interface for DAO's
 * @uses     Exception Common exceptions
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
abstract class WardenFileSystem
{
    
    /**
     * The amount of exported idea files
     * 
     * @var int
     */
    protected static $_exported = 0;

    /**
     * Directory to export de idea files
     * 
     * @access private
     */
    private $_exportDir = __DIR__ . '/../idea/';

    /**
     * Directory for exporting old idea files
     * 
     * @access private
     */
    const EXPORT_DIR_INCOMING =  __DIR__ . '/../idea/incoming/';
    
    const LOG_FILE = __DIR__ . '/../warden.log';

    /**
     * Returns an array of Warden objects instantiated from export $exportDir
     *
     * @access public
     * @return array An array of Json's idea, eg: 
     *                  ['filename1'] => [
     *                                      ["id"]  => "123abc",
     *                                      ["name"]=> "idea name",
     *                                      ...
     *                                  ],
     *                  ['filename2] => ...
     * @throws Exception
     */
    public function getIdeasFromFileSystem()
    {
        self::log("reading directory '". $this->getDir() ."'...");
        $d = $this->_getDirResource();
        $ideasArray = [];
        while (false !== ($ideaFile = readdir($d))) {
            if (!is_dir($this->_exportDir.$ideaFile) && $ideaFile != "." && $ideaFile != "..") {
                $filename = $this->_exportDir.$ideaFile;
                $f = @fopen($filename, 'r');
                $ideasArray[ $ideaFile ] 
                    = json_decode(
                        fread(
                            $f, 
                            filesize($filename)
                        ), true
                    );
            }
        }
        //print_r($ideasArray);
        closedir($d);
        return $ideasArray;
    }

    /**
     * Export Warden objects to filesystem
     *
     * @param array $wardens Array Warden objects to be exported to filesystem
     * 
     * @return void
     */
    public function export($wardens)
    {
        foreach ($wardens as $warden) {
            $filename = preg_replace(
                "/\W/", "",
                password_hash(
                    $warden->getId().
                                $warden->getName(), PASSWORD_BCRYPT
                )
            )
                        .'.'.$warden->getCount()
                        .'.idea';
            self::log("exporting '".self::EXPORT_DIR_INCOMING."$filename'...\n{$warden->__toString()}");
            $fResource = fopen(self::EXPORT_DIR_INCOMING.$filename, 'w+');
            fwrite($fResource, $warden->__toString());
            fclose($fResource);
            //copy($this->_exportDir.$filename, self::EXPORT_DIR_INCOMING.$filename);
            WardenPool::dispose($warden);
            self::$_exported++;
        }
    }

    /**
     * Gets the directory to export
     * 
     * @return string The directory for exporting idea files
     */
    public function getDir()
    {
        return self::EXPORT_DIR_INCOMING;
    }
    
    /**
     * Sets the directory to export
     * 
     * @param string $dir The directory for exporting the idea files
     * 
     * @return void
     */
    public function setDir($dir)
    {
        $this->_exportDir = $dir;
    }
    
    /**
     * Gets the directory resource
     * 
     * @return resource $d The directory resource handle
     * @throws Exception
     */
    private function _getDirResource()
    {
        if (($d = @opendir(self::EXPORT_DIR_INCOMING)) === false ) {
            self::log("Exception: Error trying to read dir ".self::EXPORT_DIR_INCOMING);
            throw new Exception("Error trying to read dir ".self::EXPORT_DIR_INCOMING, 20);
        }
        return $d;
    }
    
    /**
     * Log messages to screen and also file
     * 
     * @param string $msg The log message
     */
    public static function log($msg){
        if (filesize(self::LOG_FILE) > 100*1024*1024){
            rename(self::LOG_FILE, self::LOG_FILE.'.1');
        }
        echo '['.date('d/m/y H:i:s').']' . $msg . "\n";
        file_put_contents(self::LOG_FILE, '['.date('d/m/y H:i:s').'] ' . $msg . "\n", FILE_APPEND);
    }
    
    /**
     * Gets the number of exported idea files
     * 
     * @return int The number of exported idea files
     */
    public static function getExported(){
        return self::$_exported;
    }
    
}
