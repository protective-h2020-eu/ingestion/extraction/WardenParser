<?php
/**
 * Crate DAO which transform Crate records to \warden\src\AmavisMessage objects
 *
 * @category DAO
 * @package  DAO
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
namespace warden;
use warden\AlertDAO;
use warden\AbstractDAO;
use warden\DAO;
use warden\AmavisMessage;
use Crate\PDO\PDO as CratePDO;
use PDO;
use Crate\PDO\Exception\PDOException as CratePDOException;
use warden\WardenFileSystem;
/**
 * Crate DAO which transform Crate records to \warden\src\AmavisMessage objects
 *
 * @category DAO
 * @package  DAO
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @access   public
 * @uses     PDO The PHP Data Object class
 * @uses     Exception Common exceptions
 * @uses     \warden\src\DAO The interface for DAO's
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
class AmavisMessageDAO extends AbstractDAO implements DAO{
    
    public function __construct($database, $table, $category, $map) {
        if ($this->_pdo == NULL){
            $this->_cratePDO = new CratePDO('', $database, null, null);
            $this->_cratePDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        $this->_database = $database;
        $this->_table = $table;
        $this->_category = $category;
        $this->_map = $map;
        return $this->_cratePDO;
    }
    
    /**
     * Returns an amavis message since $time ago
     * 
     * @todo As this function is called multiple times, will be a good idea implementing Memoization
     * 
     * @param string $hash The amavis message hash related
     * 
     * @access public
     * @return AmavisMessage the amavis message object
     */
    public function findByDetectTime($time) {
        if (WardenFileSystem::getExported() >= self::LIMIT){
            return []; //limit of exported events reached
        }
        if (strtotime($time) !== false) {
            if ($this->isConnected()) {
                $sql = 'SELECT id, mailid, messageid, messageiddomain, ipadd, sender, fromaddr, senderdomain, toaddr, recipients, ts, spam, released, testlist '
                        . 'FROM amavis '
                        . 'WHERE ts > ? AND score > 6.25 AND spam = 1 AND released IS NULL '
                        . 'LIMIT ' . (self::LIMIT - WardenFileSystem::getExported());
                try{
                    $this->_cratePDO->setAttribute(PDO::ATTR_TIMEOUT, 300);
                    $query = $this->_cratePDO->prepare($sql);
                    $timestampMs = (time()-(int)$time*3600)*1000; //relative milliseconds timestamp to $time ago
                    $query->execute([$timestampMs]);
                    WardenFileSystem::log($sql.$timestampMs);
                    if ($query->rowCount() > 0){
                        $wardens = [];
                        while ($wardenData = $query->fetch(PDO::FETCH_ASSOC)) {
                            $alert = AlertDAO::getInstance()->get(sha1($wardenData['id'].$this->_table));
                            WardenFileSystem::log("processing {$wardenData['id']}...");
                            if ($alert){
                                WardenFileSystem::log("skipping alert '{$alert['id']}', already parsed...");
                                continue;
                            }
                            $warden = WardenPool::get();
                            $warden->setId(             $wardenData['id'].$this->_table);
                            $keyName = array_search('_name', $this->_map);
                            if ($keyName !== FALSE){
                                $warden->setName(       $wardenData[$keyName]);
                            }
                            $keyHash = array_search('_hash', $this->_map);
                            if ($keyHash !== FALSE && $wardenData[$keyHash]){
                                //maps the 3 first characters of the rule hits
                                $mappedHashes = array_map(function($v){
                                    return substr($v, 0, 3);
                                }, $wardenData[$keyHash]);
                                //try to get the hash value by these patterns 'H1:', 'H2:' or 'H3:'
                                if (!$hashKey = array_search('H1:', $mappedHashes)){
                                    if (!$hashKey = array_search('H2:', $mappedHashes)){
                                        $hashKey = array_search('H3:', $mappedHashes);
                                    }
                                }
                                $warden->setHash(       substr($wardenData[$keyHash][$hashKey], 3));
                            }
                            $keyDetectTime = array_search('_detectTime', $this->_map);
                            if ($keyDetectTime !== FALSE){
                                $warden->setDetectTimeMs( $wardenData[$keyDetectTime]);
                            }
                            $keyHex = array_search('_hex', $this->_map);
                            if ($keyHex !== FALSE){
                                $warden->setHex(        $wardenData[$keyHex]);
                            }
                            $keySourceIP = array_search('_sourceIP', $this->_map);
                            if ($keySourceIP !== FALSE){
                                $warden->setSourceIP(   $wardenData[$keySourceIP]);
                            }
                            $keyEmail = array_search('_email', $this->_map);
                            if ($keyEmail !== FALSE){
                                $warden->setEmail(      $wardenData[$keyEmail]);
                            }
                            //if rules hit URIBL_PHISHY, is spam and not released...
                            if ($keyHash !== FALSE && $wardenData[$keyHash] && in_array('EL_LOOKUP_URL_FULL', $wardenData[$keyHash]) && $wardenData['spam'] && !$wardenData['released']){
                                WardenFileSystem::log("setting {$wardenData['id']} as phishing...");
                                $warden->setCategory(IdeaCategory::FRAUD_PHISHING); //...then set Idea event category to spam...
                            }else{
                                $warden->setCategory(   $this->_category); //...otherwise, set it to the one defined in databases.ini
                            }
                            $wardens[] = $warden;
                        }
                    }
                } catch (CratePDOException $e){
                    WardenFileSystem::log("Crate Connection problems!" . $e->getMessage() . $e->getTraceAsString());
                }
                return $wardens;
            }else{
                WardenFileSystem::log("Crate Connection problems!");
                throw new Exception("Crate Connection problems!", 43);
            }
        }
    }

    /**
     * Returns the connection status
     * 
     * @access public
     * 
     * @return boolean :) connected or :( disconnected
     */
    public function isConnected() {
        if ($this->_cratePDO == null) {
            return false;
        }
        try{
            return (boolean) $this->_cratePDO->query('SELECT 1');
        } catch (CratePDOException $ex) {
            return false;
        }
    }

}
