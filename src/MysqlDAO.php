<?php
/**
 * Mysql DAO which transform Mysql records to \warden\src\Warden objects
 *
 * @category DAO
 * @package  DAO
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
namespace warden;
use PDO;
use PDOException;
use Exception;
use warden\AbstractDAO;
use warden\DAO;
use warden\WardenFileSystem;
/**
 * Mysql DAO which transform Mysql records to \warden\src\Warden objects
 *
 * @category DAO
 * @package  DAO
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @access   public
 * @uses     PDO The PHP Data Object class
 * @uses     Exception Common exceptions
 * @uses     \warden\src\DAO The interface for DAO's
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
class MysqlDAO extends AbstractDAO implements DAO{
    
    /**
     * Create a Mysql Connection
     * 
     * @param string $database The database name
     * @param string $table    The table name
     * @param mixed  $category The idea category according to \warden\src\IdeaCategory
     * @param array  $map      Map table fields to Warden class attributes, eg:
     *                         [//Table_field  =>  WardenClassAttribute
     *                         'mail_id'       =>  'id', 
     *                         'file_name'     =>  'name', 
     *                         'md5'           =>  'hash',
     *                         'date_modified' =>  'detectTime' ]
     * 
     * @throws Exception
     * @return void
     */
    public function __construct($database, $table, $category, $map)
    {
        try{
            /**
             * Set in development environment
             * 
             * @todo change it
             */
            $this->_pdo = new PDO(
                "mysql:host=;dbname=$database",
                '',
                ''
            );
            $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->_pdo->setAttribute(PDO::ATTR_PERSISTENT, true);
            /**
             * Set in production environment
             * 
             * @todo change it
            $this->pdo = new PDO("",
                                '',
                                '');
             */
        } catch (PDOException $e){
            WardenFileSystem::log("Connection problems! " . $e->getTraceAsString());
        }
        $this->_database = $database;
        $this->_table = $table;
        $this->_category = $category;
        $this->_map = $map;
    }
    
    public function findByDetectTime($time) {
        if (!$this->isConnected()){
            /**
             * @todo We have a possible bug here, after a while we get an error '2006 - Mysql server has gone away', reconnecting bellow fix it
             * @see https://dev.mysql.com/doc/refman/8.0/en/gone-away.html
             */
            try{
                $this->_pdo = new PDO(
                    "mysql:host=localhost;dbname=$this->_database",
                    '',
                    ''
                );
                $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->_pdo->setAttribute(PDO::ATTR_PERSISTENT, true);
            } catch (PDOException $e){
                WardenFileSystem::log('Tried to reconnect: ' . $e->getTraceAsString());
            }
            WardenFileSystem::log("Mysql Connection problems!");
            throw new Exception("Mysql Connection problems!", 40);
        }
        return parent::findByDetectTime($time);
    }
    
}
