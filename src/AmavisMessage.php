<?php

/*
 * AmavisMessage model
 */

namespace warden;

/**
 * AmavisMessage model
 *
 * @author jorgeley.oliveira
 */
class AmavisMessage {
    
    /**
     * The Amavis Id
     *
     * @var int
     * @access private
     */
    private $_id;
    
    /**
     * The Amavis from address
     *
     * @var string
     * @access private
     */
    private $_fromAddr;
    
    /**
     * The Amavis IP address
     *
     * @var string
     * @access private
     */
    private $_ipAdd;
    
    /**
     * The Amavis mail ID
     *
     * @var string
     * @access private
     */
    private $_mailId;
    
    /**
     * The Amavis message ID
     *
     * @var string
     * @access private
     */
    private $_messageId;
    
    /**
     * The Amavis message ID domain
     *
     * @var string
     * @access private
     */
    private $_messageIdDomain;
    
    /**
     * The Amavis recipients addresses
     *
     * @var string
     * @access private
     */
    private $_recipients;
    
    /**
     * The Amavis sender address
     *
     * @var string
     * @access private
     */
    private $_sender;
    
    /**
     * The Amavis sender domain
     *
     * @var string
     * @access private
     */
    private $_senderDomain;
    
    /**
     * The Amavis main recipient address
     *
     * @var string
     * @access private
     */
    private $_toAddr;
    
    /**
     * The amavis array rule hits
     *
     * @var array
     * @access private
     */
    private $_testlist;
    
    /**
     * The spam certainty (1=spam)
     *
     * @var int
     * @access private
     */
    private $_spam;
    
    /**
     * The release status (1=released, otherwise null)
     *
     * @var int
     * @access private
     */
    private $_released;
    
    /**
     * The Amavis processed timestamp
     *
     * @var int
     * @access private
     */
    private $_ts;
    
    /**
     * Serializes this class instance itself
     * 
     * @return string The serialized AmavisMessage object
     */
    public function __toString() {
        return serialize($this);
    }

    
    /**
     * Gets the Amavis ID
     * 
     * @return string The amavis ID
     */
    function getId() {
        return $this->_id;
    }

    /**
     * Gets the Amavis from address
     * 
     * @return string The Amavis from address
     */
    function getFromAddr() {
        return $this->_fromAddr;
    }

    /**
     * Gets the Amavis IP address
     * 
     * @return string The Amavis IP address
     */
    function getIpAdd() {
        return $this->_ipAdd;
    }

    /**
     * Gets the Amavis mail ID
     * 
     * @return string the Amavis mail ID
     */
    function getMailId() {
        return $this->_mailId;
    }

    /**
     * Gets the Amavis message ID
     * 
     * @return string The Amavis message ID
     */
    function getMessageId() {
        return $this->_messageId;
    }

    /**
     * Gets the Amavis message ID domain
     * 
     * @return string The message ID domain
     */
    function getMessageIdDomain() {
        return $this->_messageIdDomain;
    }

    /**
     * Gets the Amavis recipients addresses
     * 
     * @return string The Amavis recipients addresses
     */
    function getRecipients() {
        return $this->_recipients;
    }

    /**
     * Gets the Amavis sender address
     * 
     * @return string The Amavis sender address
     */
    function getSender() {
        return $this->_sender;
    }

    /**
     * Gets the Amavis sender domain address
     * 
     * @return string The Amavis sender domain address
     */
    function getSenderDomain() {
        return $this->_senderDomain;
    }

    /**
     * Gets the Amavis main recipient address
     * 
     * @return string The Amavis main recipient address
     */
    function getToAddr() {
        return $this->_toAddr;
    }

    /**
     * Gets the Amavis processed timestamp
     * 
     * @return int The Amavis processed timestamp
     */
    function getTs() {
        return $this->_ts;
    }
    
    /**
     * Gets the amavis rule hits
     * 
     * @return array The amavis rule hits
     */
    function getTestlist() {
        return $this->_testlist;
    }

    /**
     * Gets the spam verdict
     * 
     * @return int The spam verdict (1 or null)
     */
    function isSpam() {
        return $this->_spam;
    }

    /**
     * Gets the release status
     * 
     * @return int The release status
     */
    function isReleased() {
        return $this->_released;
    }

    
    /**
     * Sets the Amavis ID
     * 
     * @param string $id The Amavis ID to be set
     */
    function setId($id) {
        $this->_id = $id;
    }

    /**
     * Sets the Amavis from address
     * 
     * @param string $fromAddr The Amavis from address to be set
     */
    function setFromAddr($fromAddr) {
        $this->_fromAddr = $fromAddr;
    }

    /**
     * Sets the Amavis IP address
     * 
     * @param string $ipAdd The Amavis IP address to be set
     */
    function setIpAdd($ipAdd) {
        $this->_ipAdd = $ipAdd;
    }

    /**
     * Sets the Amavis mail ID
     * 
     * @param string $mailId The Amavis mail ID to be set
     */
    function setMailId($mailId) {
        $this->_mailId = $mailId;
    }

    /**
     * Sets the Amavis message ID
     * 
     * @param string $messageId The Amavis message ID to be set
     */
    function setMessageId($messageId) {
        $this->_messageId = $messageId;
    }

    /**
     * Sets the Amavis message ID domain
     * 
     * @param string $messageIdDomain The Amavis message ID domain to be set
     */
    function setMessageIdDomain($messageIdDomain) {
        $this->_messageIdDomain = $messageIdDomain;
    }

    /**
     * Sets the Amavis recipients addresses
     * 
     * @param string $recipients The Amavis message recipients addresses to be set
     */
    function setRecipients($recipients) {
        $this->_recipients = $recipients;
    }

    /**
     * Sets the Amavis sender address
     * 
     * @param string $sender The Amavis sender address to be set
     */
    function setSender($sender) {
        $this->_sender = $sender;
    }

    /**
     * Sets the Amavis sender domain address
     * 
     * @param string $senderDomain The Amavis sender domain address to be set
     */
    function setSenderDomain($senderDomain) {
        $this->_senderDomain = $senderDomain;
    }

    /**
     * Sets the Amavis main recipient address
     * 
     * @param string $toAddr The Amavis main recipient address to be set
     */
    function setToAddr($toAddr) {
        $this->_toAddr = $toAddr;
    }

    /**
     * Sets the Amavis processed timestamp
     * 
     * @param int $ts The Amavis processed timestamp to be set
     */
    function setTs($ts) {
        $this->_ts = $ts;
    }

    /**
     * Sets the Amavis rule hits
     * 
     * @param array $testlist The array of rule hits to be set (overwrites the previous value)
     */
    function setTestlist($testlist) {
        $this->_testlist = $testlist;
    }

    /**
     * Sets the spam verdict
     * 
     * @param int $spam The spam verdict (1 or 0)
     */
    function setSpam($spam) {
        $this->_spam = $spam;
    }

    /**
     * Sets the release status
     * 
     * @param int $released The release status (1 or null)
     */
    function setReleased($released) {
        $this->_released = $released;
    }

}
