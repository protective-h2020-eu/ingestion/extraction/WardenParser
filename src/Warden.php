<?php
/**
 * Class that represents the 'idea' file
 *
 * @category Entity
 * @package  Entity
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
namespace warden;
use Exception;
use warden\IdeaCategory;
use warden\WardenFileSystem;
/**
 * Class that represents the 'idea' file
 *
 * @category Entity
 * @package  Entity
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @access   public
 * @uses     \warden\src\IdeaCategory The Idea Categories
 * @uses     \JsonSerializable Interface for serializable elements
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
class Warden implements \JsonSerializable
{

    /**
     * Idea identifier
     * 
     * @access private
     */
    private $_id;

    /**
     * Idea name
     * 
     * @access private
     */
    private $_name = 'ideaFile';

    /**
     * Hash spam, malware, etc signature
     * 
     * @access private
     */
    private $_hash;

    /**
     * The idea format, it seens that it has just this option
     * 
     * @access private
     */
    private $_format = 'IDEA0';
    
    /**
     * Timestamp of detection in format: 1996-12-19T16:39:57-08:00
     * 
     * @access private
     * @see    https://tools.ietf.org/html/rfc3339
     */
    private $_detectTime;
    
    /**
     * Array Constant idea category
     * 
     * @access private
     * @see    \warden\src\IdeaCategory
     */
    private $_category;
    
    /**
     * How many times the incident happened
     * 
     * @access private
     */
    private $_count = 1;
    
    /**
     * Hexadecimal malware,virus, etc signature
     * 
     * @access private
     */
    private $_hex;
    
    /**
     * Confidence of the detection, 0 = false positive, default =1
     * 
     * @access private
     */
    private $_active = 1;
    
    /**
     * The source IP of the threat
     *
     * @access private
     */
    private $_sourceIP;
    
    /**
     * The source email address of the threat
     *
     * @access private
     */
    private $_email;
    
    /**
     * Just in case if someone try to echo the object
     * 
     * @return string This encoded JSON serializable class
     */
    public function __toString()
    {
        return json_encode($this->jsonSerialize());
    }
    
    /**
     * JSON idea file representation
     * 
     * @return array An array within the class attributes
     */
    public function jsonSerialize()
    {
        $jsonData = [
                    "ID"            =>  $this->getId(),
                    "Format"        =>  $this->getFormat(),
                    "DetectTime"    =>  $this->getDetectTime(),
                    "Category"      =>  $this->getCategory(),
                    "Description"   =>  substr(strstr($this->getCategory()[0], '.'), 1),
                    "Confidence"    =>  $this->getActive(),
                    //this structure [array()] is necessary for the idea pattern
                    /**
                     * @todo better not doing this to avoid empty values, better conditionally merging the arrays
                     * "Attach"        =>  [array(
                                        "Hash"      =>  [ $this->getHash() ],
                                        "Content"   =>  $this->getHex(),
                                        "Filename"  =>  [ $this->getName() ],
                                    )],
                     "Source"        =>  [[
                                        "IP4"       =>  [ $this->getSourceIP() ],
                                        "Email"     =>  [ $this->getEmail() ]
                                        ]],*/
                    "_count"        => $this->getCount()
        ];
        $attach = [
            "Hash"      =>  [ $this->getHash() ],
            "Filename"  =>  [ $this->getName() ],
            "Note"      =>  "A spam fingerprint (spam signature of email body), it's hashed using spamassassin plugin 'ixhash'"
        ];
        if ($this->getHex() !== NULL && $this->getHex() !== '' && $this->getHex() !== 'None'){
            $attach['Content'] = $this->getHex();
        }
        $jsonData = array_merge($jsonData, ['Attach' => [$attach]]);
        $source = [];
        if ($this->getSourceIP()){
            $source['IP4'] = [ $this->getSourceIP() ];
        }
        if ($this->getEmail()){
            $source['Email'] = [ $this->getEmail() ];
        }
        if ($source){
            $jsonData = array_merge($jsonData, ['Source' => [$source]]);
        }
                    /**/
        /**
         * @todo Not acceptable by Inspector module in Protective
         * change null values by reference
        array_walk_recursive(
            $jsonData,
            function (&$value) {
                    $value = ($value == null) ? '' : $value;
            }
        );*/
        return $jsonData;
    }

    /**
     * Gets the the IDEA id
     * 
     * @access public
     * @return The idea identifier
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Gets the the IDEA name
     * 
     * @access public
     * @return The idea file name
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Gets the the IDEA hash
     * 
     * @access public
     * @return The Hash spam, malware, etc signature
     */
    public function getHash()
    {
        return $this->_hash;
    }
    
    /**
     * Gets the the IDEA format
     * 
     * @access public
     * @return The idea format
     */
    public function getFormat()
    {
        return $this->_format;
    }
    
    /**
     * Gets the the IDEA detect time
     * 
     * @access public
     * @return The time of detection
     */
    public function getDetectTime()
    {
        return $this->_detectTime;
    }
    
    /**
     * Gets the the IDEA category
     * 
     * @access public
     * @return array An array in format ['Category.Subcategory', 'Description']
     */
    public function getCategory()
    {
        return $this->_category;
    }
    
    /**
     * Gets the the IDEA count
     * 
     * @access public
     * @return How many times the incident happened
     */
    public function getCount()
    {
        return $this->_count;
    }
    
    /**
     * Gets the the IDEA hexadecimal
     * 
     * @return Hexadecimal malware, virus, etc signature
     */
    public function getHex()
    {
        return $this->_hex;
    }
    
    /**
     * Gets the the IDEA status
     * 
     * @access public
     * @return The confidence of the detection, 0 = false positive, default = 1
     */
    public function getActive()
    {
        return $this->_active;
    }
    
    /**
     * Gets the source IP of the threat
     * 
     * @access public
     * @return string The Source IP of the threat
     */
    function getSourceIP() {
        return $this->_sourceIP;
    }
    
    /**
     * Gets the email address of the threat
     * 
     * @access public
     * @return string The email address of the threat
     */
    function getEmail() {
        return $this->_email;
    }

    /**
     * Sets the idea identifier
     * 
     * @param string $id The string identifier in the format ^[a-zA-Z0-9._-]+$^
     * 
     * @access public
     * @throws Exception
     * @return void
     */
    public function setId($id)
    {
        if (preg_match("^[a-zA-Z0-9._-]+$^", $id) == 0) {
            $msg = "The ID is not valid!\n May thus contain only "
                . "alphanumeric, dot, minus sign and underscore";
            WardenFileSystem::log($msg);
            throw new Exception($msg, 30);
        }
        $this->_id = sha1($id);
    }

    /**
     * Sets the idea name
     * 
     * @param string $name The string name
     * 
     * @access public
     * @return void
     */
    public function setName($name='ideaFile')
    {
        $this->_name = $name;
    }

    /**
     * Sets the hash spam, malware, etc signature
     * 
     * @param string $hash The hash string in the format (sha1 or md5): sha1:794467071687f7c59d033f4de5ece6b46415b633
     * 
     * @access public
     * @throws Exception
     * @return void
     */
    public function setHash($hash)
    {
        /**
         * @todo Hashes from databases are not passing throught this validation, needs investigation
        if (preg_match("^[a-zA-Z][a-zA-Z0-9+.-]*:[][a-zA-Z0-9._~:/?#@*'&'()*+,;=%-]*$^", $hash) == 0) {
            throw new Exception(
                "This hash is invalid!\n"
                                . "Sample formats accepted:\n"
                                . "sha1:794467071687f7c59d033f4de5ece6b46415b633\n"
                . "md5:dc89f0b4ff9bd3b061dd66bb66c991b1", 33
            );
        }
         */
        $this->_hash = 'md5:'.$hash;
    }
    
    /**
     * Sets the idea format, it seens that will be allways "IDEA0"
     * 
     * @param string $format The IDEA format
     * 
     * @return void
     */
    public function setFormat($format="IDEA0")
    {
        $this->_format = $format;
    }
    
    /**
     * Sets the idea detect time
     * 
     * @param string $detectTime The string detect time in the format: 1996-12-19T16:39:57-08:00
     * 
     * @throws Exception
     * @return void
     */
    public function setDetectTime($detectTime)
    {
        $pattern = "^[0-9]{4}-[0-9]{2}-[0-9]{2}[Tt ][0-9]{2}:[0-9]{2}:[0-9]{2}(?:\\.[0-9]+)?(?:[Zz]|(?:[+-][0-9]{2}:[0-9]{2}))?$^";
        if (preg_match($pattern, $detectTime) == 0) {
            $msg = "$detectTime is not a valid Timestamp!\n"
                . "Sample format accepted: 1996-12-19T16:39:57-08:00";
            WardenFileSystem::log($msg);
            throw new Exception($msg, 32);
        }
        $this->_detectTime = date('c', strtotime($detectTime));
    }
    
    /**
     * Sets the detect time in milliseconds
     * @todo Add some logic validation
     * 
     * @param type $detectTime The detect time in milliseconds
     */
    public function setDetectTimeMs($detectTime){
        $this->_detectTime = date('c', $detectTime / 1000);
    }
    
    /**
     * Sets the idea category
     * 
     * @param array $category One of the categories in IdeaCategory class
     * 
     * @see    \warden\src\IdeaCategory
     * @throws Exception
     * @return void
     */
    public function setCategory($category)
    {
        if (!is_array($category)) {
                /** 
                 * Think about it 
                 * 
                 * @todo || (!in_array($category, IdeaCategory::getConstantsAll()))
                 */
            $msg = "This Idea Category is invalid!\n "
                    . "Use one of the IdeaCategory class constants:\n"
                    . implode(', ', IdeaCategory::getConstantsNames());
            WardenFileSystem::log($msg);
            throw new Exception($msg, 31);
        }
        $this->_category = [ $category[0],
                            /**
                             * Remove this when Cesnet switch us to production
                             * 
                             * @todo change this
                             */
                            //IdeaCategory::TEST[0]
                          ];
    }
    
    /**
     * Set how many times this incident happened
     * 
     * @param int $count The integer count
     * 
     * @return void
     */
    public function setCount($count)
    {
        if (is_int($count)) {
            $this->_count = $count;
            //throw new Exception("This is not an integer number!", 33);
        }
    }
    
    /**
     * Set the hexadecimal virus, malware, etc signature
     * 
     * @param string $hexadecimal The hexadecimal
     * 
     * @return void
     */
    public function setHex($hexadecimal)
    {
        $this->_hex = $hexadecimal;
    }
    
    /**
     * Set The confidence of the detection, 0 = false positive, default = 1
     * 
     * @param boolean $active TRUE = active, FALSE = inactive
     * 
     * @return void
     */
    public function setActive($active)
    {
        if (is_bool($active)) {
            $this->_active = $active;
            //throw new Exception("This is not an integer number!", 33);
        }
    }
    
    /**
     * Sets the source IP of the threat
     * 
     * @param string $sourceIP
     */
    function setSourceIP($sourceIP) {
        $this->_sourceIP = $sourceIP;
    }

    /**
     * Sets the email address of the threat
     * 
     * @param string $email An email address where the threat comes from
     * 
     * @return void
     */
    function setEmail($email) {
        $this->_email = $email;
    }

}
