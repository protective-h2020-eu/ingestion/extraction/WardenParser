<?php
/**
 * Factory class responsible for instantiating the specific DAO's for 
 * each data source, eg: Mysql, SQL Server, etc
 * 
 * @category DAO
 * @package  DAO
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
namespace warden;
use PDOException;
use Exception;
use warden\MysqlDAO;
use warden\WardenFileSystem;
/**
 * Factory class responsible for instantiating the specific DAO's for 
 * each data source, eg: Mysql, SQL Server, etc
 *
 * @abstract
 * @category DAO
 * @package  DAO
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @access   public
 * @uses     Exception Common exceptions
 * @uses     \warden\src\MysqlDAO The Mysql Data Access Object
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
abstract class DAOFactory
{
    private static $_dbMaps;
    
    /**
     * PDO Protective Postgres instance
     *
     * @access private
     * @var \PDO
     */
    private static $_protectivePostgres;
    
    /**
     * Protective Postgres DAO constant
     * 
     * @access public
     */
    const PROTECTIVE_POSTGRES = 1;
    
    /**
     * Crate DB Amavis constant
     * 
     * @access public
     */
    const CRATE_AMAVIS = 2;

    /**
     * Constant for instantiating a Mysql database 'av_signatures', table 
     * 'custom_signatures' MysqlDAO object
     *
     * @access public
     * @var    int
     */
    const MYSQL_AVCUSTOMSIGNATURES = 3;

    /**
     * Constant for instantiating a Mysql database 'av_signatures', table 
     * 'url_signatures' MysqlDAO object
     *
     * @access public
     * @var    int
     */
    const MYSQL_AVURLSIGNATURES = 4;

    /**
     * Constant for instantiating a Mysql database 'hashdb', table 
     * 'emailhashes' MysqlDAO object
     *
     * @access public
     * @var    int
     */
    const MYSQL_EMAILHASHES = 5;

    /**
     * Constant for instantiating a Mysql database 'hashdb', table 
     * 'hashes' MysqlDAO object
     *
     * @access public
     * @var    int
     */
    const MYSQL_HASHES = 6;

    /**
     * Constant for instantiating a Mysql database 'spamdb', table 
     * 'hashes' MysqlDAO object
     *
     * @access public
     * @var    int
     */
    const MYSQL_SPAMHASHES = 7;
    
    /**
     * Constant for getting all databases specified in databases.ini file
     *
     * @access public
     * @var    int
     */
    const ALL = 99;

    /**
     * Returns a DAO's instance according to the $dao param
     *
     * @param int $dao The DAOFactory constant
     * 
     * @access public
     * @static
     * @return MysqlDAO The MysqlDao object for the specific $dao constant
     * @throws Exception
     */
    public static function getDAO($dao) 
    {
        switch ($dao) {
                    
        //Protective Postgres DAO
        case self::PROTECTIVE_POSTGRES:
            if (self::$_protectivePostgres == NULL){
                $dsn = 'pgsql:host='
                        . ';port='
                        . ';user='
                        . ';dbname=';
                try{
                    self::$_protectivePostgres = new \PDO($dsn);
                    self::$_protectivePostgres->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                } catch (PDOException $e){
                    WardenFileSystem::log('Failed to connect to mentat database. ' . $e->getMessage());
                }
            }
            return self::$_protectivePostgres;
            break;
            
        //Crate DB Amavis DAO
        case self::CRATE_AMAVIS:
            return new AmavisMessageDAO(
                'crate', //database
                'amavis', //table
                IdeaCategory::ABUSIVE_SPAM, //category
                self::getDBini()['crate']['amavis'] //table_fileds=>WardenClassAttributes
            );
            break;
        /**
         * Not parsing this for now
        case self::MYSQL_AVCUSTOMSIGNATURES:
            return new MysqlDAO(
                'av_signatures', //database
                'custom_signatures', //table
                IdeaCategory::MALWARE_GENERAL, //category
                [// Table_field     =>  WardenClassAttribute
                    'id'            => '_id',
                    'mail_id'       => '_name',
                    'md5'           => '_hash',
                    'hex'           => '_hex',
                    'count'         => '_count',
                    'active'        => '_active',
                    'date_modified' => '_detectTime'
                ]
            );
                break;
        case self::MYSQL_AVURLSIGNATURES:
            return new MysqlDAO(
                'av_signatures', //database
                'url_signatures', //table
                IdeaCategory::MALWARE_GENERAL, //category
                [// Table_field     =>  WardenClassAttribute
                    'id'            => '_id',
                    'item_id'       => '_name',
                    'md5'           => '_hash',
                    'hex'           => '_hex',
                    'count'         => '_count',
                    'active'        => '_active',
                    'ip'            => '_sourceIP',
                    'date_modified' => '_detectTime'
                ]
            );
                break;
        case self::MYSQL_EMAILHASHES:
            return new MysqlDAO(
                'hashdb', //database
                'emailhashes', //table
                IdeaCategory::SPAM_SENDING_ADDRESS, //category
                [// Table_field   =>  WardenClassAttribute
                    'id'            => '_id',
                    //'email'       =>  'name',
                    'hash'          => '_hash',
                    'count'         => '_count',
                    'last_update'   => '_detectTime'
                ]
            );
                break;
         */
        case self::ALL:
            return self::_getAllDAOs();
                break;
            /**
             * Not parsing this for now
              case self::MYSQL_HASHES:
              return new MysqlDAO(
              'hashdb', //database
              'hashes', //table
              [// Table_field   =>  WardenClassAttribute
              'id'          =>  'id',
              'hash'        =>  'hash',
              'count'       =>  'count',
              'last_update' =>  'detectTime'
              ]
              );
              break;
              case self::MYSQL_SPAMHASHES:
              return new MysqlDAO(
              'spamdb', //database
              'hashes', //table
              [// Table_field   =>  WardenClassAttribute
              'id'          =>  'id',
              'hash'        =>  'hash',
              'count'       =>  'count',
              'last_update' =>  'detectTime'
              ]
              );
              break;
             */
            
            /**
             * New DAO's coming soon
             * 
             * @todo implement them following the pattern above
             */
        default:
            WardenFileSystem::log('This DAO is not implemented!!!');
            throw new Exception('This DAO is not implemented!!!', 1);
        }
    }

    /**
     * Get all Databases tables defined in databases.ini file
     *
     * @see    databases.ini
     * @return MysqlDAO array of MysqlDAO objects
     * @throws Exception
     */
    private static function _getAllDAOs() 
    {
        try {
            $daos = [];
            $daos[] = self::getDAO(self::CRATE_AMAVIS);
            return $daos;
        } catch (Exception $e) {
            WardenFileSystem::log('Something is wrong with your databases.ini file!');
            throw new Exception('Something is wrong with your databases.ini file!');
        }
    }
    
    private static function getDBini(){
        if (self::$_dbMaps == NULL){
            self::$_dbMaps = parse_ini_file('databases.ini', true);
        }
        return self::$_dbMaps;
    }

}
