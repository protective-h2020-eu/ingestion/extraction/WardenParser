<?php
/**
 * Mysql DAO which transform Mysql records to \warden\src\Warden objects
 *
 * @category DAO
 * @package  DAO
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
namespace warden;
use PDO;
use PDOException;
use Exception;
use warden\DAO;
use warden\AlertDAO;
use warden\WardenFileSystem;
/**
 * Mysql DAO which transform Mysql records to \warden\src\Warden objects
 *
 * @category DAO
 * @package  DAO
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @access   public
 * @uses     PDO The PHP Data Object class
 * @uses     Exception Common exceptions
 * @uses     \warden\src\DAO The interface for DAO's
 * @link     http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 */
abstract class AbstractDAO implements DAO
{
    
    /**
     * Default limit of events
     */
    const LIMIT = 1000;

    /**
     * \PDO The PDO object connection
     */
    protected $_pdo;
    
    /**
     * String The database name
     */
    protected $_database;
    
    /**
     * String The table name
     */
    protected $_table;
    
    /**
     * Mixed Array idea category according to \warden\src\IdeaCategory
     */
    protected $_category;

    /**
     * Array A map from table field to Warden class attribute
     */
    protected $_map;

    /**
     * Retrieve unique Idea's from 'av_signatures' database table 'custom_signatures'
     * that were not already exported before
     *
     * @param array $wardensIds Array of ID's already exported
     * 
     * @return array An array of Warden unique objects
     * @throws Exception
     */
    public function findByUniqueWardens($wardensIds)
    {
        if (count($wardensIds) == 0) {
            $wardensIds = array('all');
        }
        if ($this->isConnected()) {
            $placeHolders = implode(',', array_fill(0, count($wardensIds), '?'));
            $sqlStatement = $this->_getBaseSQLStatement()
                            .' WHERE ' . array_search('_id', $this->_map)
                            ." NOT IN ($placeHolders)"
                            . " LIMIT " . (self::LIMIT - WardenFileSystem::getExported());
            return $this->getWardensFromPDO($sqlStatement, $wardensIds);
        } else {
            WardenFileSystem::log("Mysql Connection problems!");
            throw new Exception("Mysql Connection problems!", 40);
        }
    }
    
    /**
     * Retrieve new Wardens since last execution specified by $time parameter, eg: 
     *                  00:30:00 (Wardens whom 'date_modified' table field were 
     *                            updated 30 minutes ago will be retrieved)
     *
     * @param string $time The time since last execution, use format: hh:mm:ss
     * 
     * @return array An array of Warden unique objects
     * @throws Exception
     */
    public function findByDetectTime($time)
    {
        if (WardenFileSystem::getExported() >= self::LIMIT){
            return []; //limit of exported events reached
        }
        if (strtotime($time) !== false) {
            if ($this->isConnected()) {
                $sqlStatement = $this->_getBaseSQLStatement()
                                .' WHERE ' . array_search('_detectTime', $this->_map)
                                .' > ?'
                                . ' LIMIT ' . (self::LIMIT - WardenFileSystem::getExported());
                $time = $this->getDBSubTime($time);
                return $this->getWardensFromPDO($sqlStatement, array($time));
            }
        } else {
            WardenFileSystem::log("This is not a valid time, use the format: hh:mm:ss");
            throw new Exception("This is not a valid time, use the format: hh:mm:ss", 41);
        }
    }
    
    /**
     * The initial base SQL
     * 
     * @return string The initial base SQL
     */
    private function _getBaseSQLStatement()
    {
            $fieldsAs = array_map( //CONVERT() is to make sure that all chars are ASCII
                            function ($jsonField, $tableField) {
                                return 'CONVERT('.$tableField.' USING ASCII) AS '.$jsonField;
                            },
                $this->_map, 
                array_keys($this->_map)
            );
            return "SELECT " .implode(',', $fieldsAs)
                            ." FROM " .$this->_table;
    }

    /**
     * Query the database and return Warden objects from it
     *
     * @param string $sqlStatement The SQL statement to be executed
     * @param array  $params       The SQL param for the statement
     * 
     * @return array Array of Warden objects
     */
    public function getWardensFromPDO($sqlStatement, $params=[''])
    {
        if (is_array($params)) {
            try{
                $query = $this->_pdo->prepare($sqlStatement);
                //Parameter avoid query objects already exported, improving performance
                $query->execute($params);
                WardenFileSystem::log($query->queryString."[{$params[0]}]");
                //getting the objects from database
                $wardens = [];
                while ($wardenData = $query->fetch(PDO::FETCH_ASSOC)) {
                    $alert = AlertDAO::getInstance()->get(sha1($wardenData['_id'].$this->_table));
                    WardenFileSystem::log("alert: " . print_r($alert, TRUE));
                    if ($alert){
                        WardenFileSystem::log("skipping alert '{$alert['id']}', already parsed...");
                        continue;
                    }
                    $warden = WardenPool::get();
                    $warden->setId(             $wardenData['_id'].$this->_table);
                    if (array_search('_name', $this->_map) !== FALSE){
                        $warden->setName(       $wardenData['_name']);
                    }
                    if (array_search('_hash', $this->_map) !== FALSE){
                        $warden->setHash(       $wardenData['_hash']);
                    }
                    if (array_search('_detectTime', $this->_map) !== FALSE){
                        $warden->setDetectTime( $wardenData['_detectTime']);
                    }
                    if (array_search('_hex', $this->_map) !== FALSE){
                        $warden->setHex(        $wardenData['_hex']);
                    }
                    if (array_search('_sourceIP', $this->_map) !== FALSE){
                        $warden->setSourceIP(   $wardenData['_sourceIP']);
                    }
                    if (array_search('_email', $this->_map) !== FALSE){
                        $warden->setEmail(      $wardenData['_email']);
                    }
                    $warden->setCategory(   $this->_category); //...otherwise, set it to the one defined in databases.ini
                    $wardens[] = $warden;
                }
            } catch (PDOException $e){
                WardenFileSystem::log('Error trying to get warden objects ' . $e->getTraceAsString());
            }
            return $wardens;
        } else {
            WardenFileSystem::log("Pass the parameter as array!");
            throw new Exception("Pass the parameter as array!", 42);
        }
    }
    
    /**
     * Gets the Mysql server time
     * 
     * @return int The Mysql server time
     */
    public function getDBTime()
    {
        if ($this->_pdo == null) {
            return 0;
        }
        try{
            return $this->_pdo->query('SELECT NOW()')->fetch(PDO::FETCH_COLUMN);
        } catch (PDOException $ex) {
            WardenFileSystem::log('Error trying to get Mysql Time ' . $ex->getTraceAsString());
            return 0;
        }
    }
    
    /**
     * Gets the Mysql server time - $time
     * 
     * @param type $time The time to be subtracted
     * 
     * @return int The Mysql server time - $time
     */
    public function getDBSubTime($time)
    {
        if ($this->_pdo == null) {
            return 0;
        }
        try{
            return $this->_pdo->query("SELECT SUBTIME(NOW(), '$time')")->fetch(PDO::FETCH_COLUMN);
        } catch (PDOException $ex) {
            WardenFileSystem::log('Error trying to get Mysql Sub Time ' . $ex->getTraceAsString());
            return 0;
        }
    }

    /**
     * Check the connection
     * 
     * @return boolean The connection status
     */
    public function isConnected()
    {
        if ($this->_pdo == null) {
            WardenFileSystem::log("PDO is null " . gettype($this->_pdo));
            return false;
        }
        try{
            WardenFileSystem::log("PDO error: " . print_r($this->_pdo->errorInfo(), true));
            $connected = (boolean) $this->_pdo->query('SELECT 1');
            //WardenFileSystem::log("connected: $connected");
            return $connected;
        } catch (PDOException $ex) {
            WardenFileSystem::log($ex->getTraceAsString());
            return false;
        }
    }
}
