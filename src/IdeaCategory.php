<?php
/**
 * Classification of events for IDEA in the “Category” key
 * 
 * @category Reflection
 * @package  Reflection
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @link     https://idea.cesnet.cz/en/classifications Source
 */
namespace warden;
use ReflectionClass;
/**
 * Classification of events for IDEA in the “Category” key
 * 
 * @category Reflection
 * @package  Reflection
 * @author   Jorgeley, <jinacio@theemaillaundry.com>
 * @license  http://https://gitlab.com/protective-h2020-eu/WardenParser Protective Project
 * @access   public
 * @uses     \ReflectionClass The reflection class
 * @link     https://idea.cesnet.cz/en/classifications Source
 */
abstract class IdeaCategory
{
    
    /**
     * Get all Idea categories with their descriptions
     *
     * @return array Idea categories defined constants in format: 
     * [CONSTANT_NAME] => Array
     *  (
     *      [0] => Category.Subcategory
     *      [1] => Description
     *  )
     *  ...and more
     */
    public function getConstantsAll()
    {
        return (new ReflectionClass(get_class()))->getConstants();
    }
    
    /**
     * Get all the constant names
     * 
     * @return array Idea constants names, eg: [0=>'ABUSIVE_SPAM',1=>'more...']
     */
    public function getConstantsNames()
    {
        return array_keys(self::getConstantsAll());
    }

    const ABUSIVE_SPAM =        ['Abusive.Spam', 'Unsolicited Bulk Email, this means that the recipient has not granted verifiable permission for the message to be sent and that the message is sent as part of a larger collection of messages, all having a functionally comparable content.'];
    const ABUSIVE_HARASSMENT =  ['Abusive.Harassment', 'Discreditation or discrimination of somebody (e.g. cyberstalking, racism and threats against one or more individuals)'];
    const ABUSIVE_CHILD =       ['Abusive.Child', 'Child pornography'];
    const ABUSIVE_SEXUAL =      ['Abusive.Sexual'];
    const ABUSIVE_VIOLENCE =    ['Abusive.Violence', 'Glorification of violence'];
    const MALWARE_VIRUS =       ['Malware.Virus', 'Software that is intentionally included or inserted in a system for harmful purpose. A user interaction is normally necessary to activate the code.'];
    const MALWARE_WORM =        ['Malware.Worm'];
    const MALWARE_TROJAN =      ['Malware.Trojan'];
    const MALWARE_SPYWARE =     ['Malware.Spyware'];
    const MALWARE_DIALER =      ['Malware.Dialer'];
    const MALWARE_ROOTKIT =     ['Malware.Rootkit'];
    const MALWARE_GENERAL =     ['Malware'];
    const RECON_SCANNING =            ['Recon.Scanning', 'Attacks that send requests to a system to discover weak points. This includes also some kind of testing processes to gather information about hosts, services and accounts. Examples: fingerd, DNS querying, ICMP, SMTP (EXPN, RCPT, …), port scanning and host sweeping.'];
    const RECON_SNIFFING =            ['Recon.Sniffing', 'Sniffing,Observing and recording of network traffic (wiretapping).'];
    const RECON_SOCIALENGINEERING =   ['Recon.SocialEngineering', 'Gathering information from a human being in a non-technical way (e.g. lieas, tricks, bribes or threats).'];
    const RECON_SEARCHING =           ['Recon.Searching', 'Google hacking or suspicious searches against site.'];
    const ATTEMPT_EXPLOIT =         ['Attempt.Exploit', 'An attempt to compromise a system or to disrupt any service by exploiting vulnerabilities with a standardised identifier such as CVE name (e.g. buffer overflow, backdoors, cross site scripting, etc.).'];
    const ATTEMPT_LOGIN =           ['Attempt.Login', 'Multiple login attempts (guessing/cracking of passwords, brute force).'];
    const ATTEMPT_NEWSIGNATURE =    ['Attempt.NewSignature', 'An attempt using and unknown exploit.'];
    const INTRUSION_ADMINCOMPROMISE =     ['Intrusion.AdminCompromise', 'A successful compromise of a system or application (service). This can have been caused remote by a known or new vulnerability, but also by an unauthorized local access. Also includes being part of a botnet.'];
    const INTRUSION_USERCOMPROMISE =      ['Intrusion.UserCompromise'];
    const INTRUSION_APPCOMPROMISE =       ['Intrusion.AppCompromise'];
    const INTRUSION_BOTNET =              ['Intrusion.Botnet'];
    const AVAILABILITY_DOS =         ['Availability.DoS', 'System bombarded with so many requests (packets, connections) that the operations are delayed or the system crashes. DoS examples are ICMP and SYN floods, Teardrop attacks and mail-bombing. DDoS often is based on DoS attacks originating from botnets, but also other scenarios exist like DNS Amplification attacks.'];
    const AVAILABILITY_DDOS =        ['Availability.DDoS'];
    const AVAILABILITY_SABOTAGE =    ['Availability.Sabotage', 'Outage, caused by local actions (destruction, disruption of power supply, etc.) - willfully or caused by deliberate gross neglect.'];
    const AVAILABILITY_OUTAGE =      ['Availability.Outage', 'Outage, caused by Act of God, spontaneous failures or human error, without malice or deliberate gross neglect being involved.'];
    const INFORMATION_UNAUTHORIZEDACCESS =          ['Information.UnauthorizedAccess', 'Besides a local abuse of data and systems the information security can be endangered by a successful account or application compromise. Furthermore attacks are possible that intercept and access information during transmission (wiretapping, spoofing or hijacking). Human configuration/software error can also be the cause.'];
    const INFORMATION_UNAUTHORIZEDMODIFICATION =    ['Information.UnauthorizedModification'];
    const FRAUD_UNAUTHORIZEDUSAGE =   ['Fraud.UnauthorizedUsage', 'Using resources for unauthorized purposes including profit-making ventures (E.g. the use of e-mail to participate in illegal profit chain letters or pyramid schemes).'];
    const FRAUD_COPYRIGHT =           ['Fraud.Copyright', 'Offering or installing copies of unlicensed commercial software or other copyright protected materials (Warez).'];
    const FRAUD_MASQUERADE =          ['Fraud.Masquerade', 'Type of attacks in which one entity illegitimately assumes the identity of another in order to benefit from it.'];
    const FRAUD_PHISHING =            ['Fraud.Phishing', 'Masquerading as another entity in order to persuade the user to reveal a private credential.'];
    const FRAUD_SCAM =                ['Fraud.Scam', 'Fraud on a person by falsely gaining confidence. Prominent example is Nigerian 419 scam.'];
    const VULNERABLE_OPEN =    ['Vulnerable.Open', 'Open for abuse, solvable preferably by patch, update, etc. - vulnerabilities apparent from Nessus and similar scan.'];
    const VULNERABLE_CONFIG =  ['Vulnerable.Config', 'Open for abuse, solvable preferably by configuration hardening/fixing - open resolvers, world readable printers, virus signatures not up-to-date, etc.'];
    const ANOMALY_TRAFFIC =     ['Anomaly.Traffic', 'Anomalies not yet identified as clear security problem.'];
    const ANOMALY_CONNECTION =  ['Anomaly.Connection'];
    const ANOMALY_PROTOCOL =    ['Anomaly.Protocol'];
    const ANOMALY_SYSTEM =      ['Anomaly.System'];
    const ANOMALY_APPLICATION = ['Anomaly.Application'];
    const ANOMALY_BEHAVIOUR =   ['Anomaly.Behaviour'];
    const SPAM_SENDING_ADDRESS = ['Spam.SendingAddress'];
    const OTHER =   ['Other', 'Yet unknown/unidentified type of attack, or attack unrecognized automatically by machine.'];
    const TEST =    ['Test', 'Meant for testing.'];
}