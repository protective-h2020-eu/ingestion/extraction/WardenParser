<?php

/*
 * DAO for Alert model
 */

namespace warden;

use PDO;
use Exception;

/**
 * DAO for Alert model
 *
 * @author jorgeley.oliveira
 */
class AlertDAO{

    /**
     * This class instance itself
     *
     * @var \DAOs\AlertDAO
     * @access private
     */
    private static $_alertDAO;
    
    /**
     * The Postgres connection to Protective node
     *
     * @var \PDO
     * @access private
     */
    private $_pdo;
    
    /**
     * No one can access the constructor
     */
    private function __construct()
    {        
    }
    
    /**
     * Not even clone
     *
     * @throws CloneException
     * @return void
     */
    public function __clone()
    {
        throw new CloneException();
    }

    /**
     * Gets this class instance
     * 
     * @return AlertDAO
     */
    public static function getInstance() {
        if (self::$_alertDAO == null) {
            self::$_alertDAO = new AlertDAO();
            self::$_alertDAO->_pdo = DAOFactory::getDAO(DAOFactory::PROTECTIVE_POSTGRES);
            self::$_alertDAO->_pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);
        }
        return self::$_alertDAO;
    }
    
    /**
     * Retrieve a specific Alert by his id
     * 
     * @param string $alertId The Alert id to be retrieved
     * 
     * @return array The Alert object
     */
    public function get($alertId){
        if (is_string($alertId)){
            $query = $this->_pdo->prepare('SELECT * FROM events WHERE id = ? LIMIT 1');
            $query->execute([$alertId]);
            return $query->fetch(PDO::FETCH_ASSOC);
        }
    }

    /**
     * Gets all the alerts (careful! it's too much data!)
     * 
     * @todo It's causing memory exhausting, fix it
     * @return models\Alert An array within models\Alert objects
     */
    public function getAll() {
        $query = $this->_pdo->prepare('SELECT * FROM meta_alerts');
        $query->execute();
        $alerts = $query->fetchAll(PDO::FETCH_ASSOC);
        return $this->getAlerts($alerts);
    }

    /**
     * Retrieve all alerts since $timeWindow ago
     * 
     * @param string $timeWindow The time window, eg: '02:00:00'
     * 
     * @return array An array within instances of \models\Alert objects
     */
    public function getByTime($timeWindow) {
        if (strtotime($timeWindow) === FALSE){
            throw new Exception("The time window isn't valid, '$timeWindow' given. Use the format: 13:00:00", 34);
        }
        /**
         * @todo this query isn't accepting prepared statements, needs investigation
         */
        $query = $this->_pdo->query("SELECT * FROM meta_alerts WHERE detecttime >= NOW() - INTERVAL '$timeWindow'");
        $query->execute();
        $alerts = $query->fetchAll(PDO::FETCH_ASSOC);
        return $this->getAlerts($alerts);
    }
    
    /**
     * Retrieve all alerts for a bunch of specific categories
     * 
     * @param array  $categories An array within category defined in the class constants IdeaCategory
     * @param string $timeWindow Time window in days (careful! More than +-15 days causes memory exhaustion!)
     * @param array $excludedIPs An array of IP's which will be excluded from the query (it helps in performance)
     * 
     * @return array An array within serialized \models\Alert objects (since the results are too big, we'd better serialize them) 
     * @throws Exception
     */
    public function getByCategories($categories, $timeWindow, $excludedIPs = []){
        if (!is_array($categories)){
            throw new Exception('The $categories should be an array but "' . gettype($categories) . '" given', 77);
        }
        foreach ($categories as $category){
            if (!in_array($category, array_column(IdeaCategory::getConstantsAll(), 0))){
                throw new Exception("The category should be one of the predefined ones: " 
                        . implode(', ', array_column(IdeaCategory::getConstantsAll(), 0)) . "\n" . $category . " given.", 78);
            }
        }
        $placeHolders = implode(',', array_fill(0, count($categories), '?'));
        $sql = "SELECT * FROM meta_alerts "
                . "WHERE category && ARRAY[$placeHolders] "
                //we better query day by day rather than the whole interval to save some memory
                . "AND detecttime BETWEEN (NOW() - INTERVAL '$timeWindow days') "
                . "AND (NOW() - INTERVAL '" . ($timeWindow-1) . " days') ";
        if (count($excludedIPs) > 0){ //excluding some IPs already pushed helps the performance
            array_walk($excludedIPs, function(&$excludedIP){ //applying the psql query format for iprange type
                $excludedIP = "'$excludedIP'::iprange";
            });
            $sql .= "AND NOT(source_ip && ARRAY[" . implode(', ', $excludedIPs) . "]) ";
        }                
        $sql .= "ORDER BY source_ip LIMIT 10000";
        Log::getInstance()->setMessage(
                substr($sql, 0, 203) 
                . '...(' . count($excludedIPs) .' IPs)...' 
                . substr($sql, -47) . ' -> '
                . implode(', ', $categories))->toFile();
        $alerts = [];
        try{
            $query = $this->_pdo->prepare($sql, [
                PDO::ATTR_TIMEOUT => 300, 
                PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL, //making the query scrollable rather than fetching all helps performance
                PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false
            ]);
            $query->execute($categories);
            $PDOalerts = PDOIterator::getInstance($query); //iterating the results rather than fetching all helps performance
            while ($PDOalerts->next()){
                /** @todo The full object causes memory exhaustion */
                $alerts[] = $PDOalerts->current();
                //echo round(memory_get_usage(true)/1024/1024)."\n";
            }
            $query->closeCursor();
            $query = NULL; //to free some memory
        } catch (PDOException $e){
            Log::getInstance()
                    ->setMessage('PDOException trying to get categories')
                    ->setException($e)
                    //->report()
                    ->toFile();
        } catch (Exception $e){
            Log::getInstance()
                    ->setMessage('Exception trying to get categories')
                    ->setException($e)
                    //->report()
                    ->toFile();
        }
        return $alerts;
    }
    
    /**
     * Retrieve all alerts for a specific category
     * 
     * @param IdeaCategory $category The category defined in the class constants IdeaCategory
     * 
     * @return array An array within instances of \models\Alert objects
     * 
     * @throws Exception
     */
    public function getByCategory($category){
        if (!in_array($category, IdeaCategory::getConstantsAll())){
            throw new Exception("The category should be one of the predefined ones: " 
                    . implode(', ', IdeaCategory::getConstantsNames()) . "\n" . $category[0] . " given.", 79);
        }
        $query = $this->_pdo->prepare("SELECT * FROM meta_alerts WHERE ? = ANY(category)");
        $query->execute([$category[0]]);
        $alerts = $query->fetchAll(PDO::FETCH_ASSOC);
        return $this->getAlerts($alerts);
    }

    /**
     * Return Alert objects from an array PDO::FETCH_ASSOC query result
     * 
     * @param array $alertArrays The alerts array query result
     * 
     * @return array An array within instances of \models\Alert objects
     */
    private function getAlerts($PDOalerts) {
        $alertArrays = [];
        foreach ($PDOalerts as $PDOalert) {
            //var_dump($doc);
            $alert = new Alert();
            $alert->setId(              $PDOalert['id']);
            $alert->setCategory(        $PDOalert['category']);
            $alert->setDetectTime(      $PDOalert['detecttime']);
            if (isset($PDOalert['_count'])) {
                $alert->setCount(       $PDOalert['_count']);
            }
            $source = new Host();
            $source->setProtocol(       $PDOalert['protocol']);
            $source->setPort(           $PDOalert['source_port']);
            $source->setIp(             $PDOalert['source_ip']);
            $source->setHostname(       $PDOalert['source_hostname']);
            $alert->setSource($source);
            $target = new Host();
            $target->setProtocol(       $PDOalert['protocol']);
            $target->setPort(           $PDOalert['target_port']);
            $target->setIp(             $PDOalert['target_ip']);
            $target->setHostname(       $PDOalert['target_hostname']);
            $alert->setTarget($target);
            $alertArrays[] = $alert;
        }
        //var_dump($alerts);
        return $alertArrays;
    }

}
