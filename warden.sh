#!/bin/sh

### BEGIN INIT INFO
# Provides:          Warden Parser
# Required-Start:
# Required-Stop:
# Default-Start:
# Default-Stop:
# Short-Description: Shell Script for the Warden Parser
### END INIT INFO

set -e

case $1 in
	start)
		clear
        nohup php warden.php > /dev/null &
        nohup python2 warden_client/contrib_3.0-beta2/warden_filer/warden_filer.py -c warden_client/contrib_3.0-beta2/warden_filer/warden_filer.cfg sender > /dev/null &
		;;
	stop)
        #warden_filer_process_id=`/bin/ps -fu $USER| grep "python2 warden_client/contrib_3.0-beta2/warden_filer/warden_filer.py -c warden_client/contrib_3.0-beta2/warden_filer/warden_filer.cfg sender" | grep -v "grep" | awk '{print $2}'`
        #kill -9 $warden_filer_process_id
        #kill -9 $warden_filer_process_id
        warden_parser_process_id=`/bin/ps -fu $USER| grep "php warden.php" | grep -v "grep" | awk '{print $2}'`
		kill -9 $warden_parser_process_id
		kill -9 $warden_parser_process_id
		;;
	*)
		echo "Usage: warden.sh {start|stop}"
		exit 1
		;;
esac
