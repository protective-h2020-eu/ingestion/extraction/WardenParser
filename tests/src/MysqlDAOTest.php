<?php
namespace warden\tests\src;

use \PHPUnit\Framework\TestCase;
use warden\IdeaCategory;
use warden\MysqlDAO;
use warden\Warden;

/**
 * Unit Test for \warden\src\MysqlDAO
 */
class MysqlDAOTest extends TestCase{

    /**
     * @var MysqlDAO The Mysql DAO object
     */
    protected $mysqlDAO;
    
    /**
     * @var \PHPUnit\Framework\MockObject
     */
    protected $wardenManager;

    /**
     * Instantiate a Mysql DAO
     */
    protected function setUp(){
        $this->markTestIncomplete();
        $this->mysqlDAO = new MysqlDAO(
                                        'av_signatures', //database
                                        'custom_signatures', //table
                                        IdeaCategory::MALWARE_GENERAL,
                                        [ //map table field to Warden class attribute
                                            'mail_id'       =>  'id',
                                            'file_name'     =>  'name',
                                            'md5'           =>  'hash',
                                            'hex'           =>  'hex',
                                            'date_modified' =>  'detectTime'
                                        ]
                                    );
        $this->wardenManager = $this->getMockBuilder('warden\src\WardenManager')
                                    ->disableOriginalConstructor()
                                    ->getMock();
        $warden1 = new Warden;
        $warden1->setId(        'abc123def');
        $warden1->setName(      'mock1');
        $warden1->setHash(      'sha1:794467071687f7c59d033f4de5ece6b46415b633');
        $warden1->setHex(       'hex123hex');
        $warden1->setDetectTime('1996-12-19T16:39:57-08:00');
        $warden1->setCategory(  IdeaCategory::MALWARE_GENERAL);
        $warden1->setCount(     1);
        $warden2 = new Warden;
        $warden2->setId(        'abc456def');
        $warden2->setName(      'mock2');
        $warden2->setHash(      'sha1:794467071687f7c59d033f4de5ece6b46415b634');
        $warden2->setHex(       'hex456hex');
        $warden2->setDetectTime('1996-12-19T16:39:57-03:00');
        $warden2->setCategory(  IdeaCategory::MALWARE_GENERAL );
        $warden2->setCount(     1);
        $this->wardenManager->method('getWardens')                            
                            ->will($this->returnValue([$warden1, $warden2]));
        $this->wardenManager->method('getWardensIDs')                            
                            ->will($this->returnValue(['abc123def', 'abc456def']));
    }

    /**
     * Destroy the Mysql DAO object
     */
    protected function tearDown(){
        $this->mysqlDAO = null;
    }
    
    /**
     * Test if is connected
     * @covers \warden\src\MysqlDAO::isConnected()
     */
    public function testIsConnected(){
        $this->markTestIncomplete();
        $this->assertTrue($this->mysqlDAO->isConnected());
    }

    /**
     * @covers \warden\src\MysqlDAO::findByUniqueWardens($wardensIds)
     */
    public function testfindByUniqueWardens(){
        $this->markTestIncomplete();
        $wardensIds = $this->wardenManager->getWardensIDs();
        $wardensObjects = $this->mysqlDAO->findByUniqueWardens($wardensIds);
        $unique = true;
        foreach ($this->wardenManager->getWardens() as $warden){
            $unique = in_array($warden, $wardensObjects, true)
                    ? false 
                    : $unique;
            $this->assertInstanceOf(Warden::class, $warden);
        }
        $this->assertTrue($unique);
        $this->assertNotEmpty($wardensObjects);
    }
    
    /**
     * @covers \warden\src\MysqlDAO::findByDetectTime($time)
     * @expectedException \Exception
     * @expectedExceptionCode 41
     */
    public function testFindByDetectTimeInvalidTime(){
        $this->markTestIncomplete();
        $this->mysqlDAO->findByDetectTime('INVALID TIME');
    }

}