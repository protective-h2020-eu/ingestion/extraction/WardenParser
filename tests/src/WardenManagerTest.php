<?php
namespace warden\tests\src;

use PHPUnit\Framework\TestCase;
use \org\bovigo\vfs\vfsStreamWrapper;
use \org\bovigo\vfs\vfsStreamDirectory;
use \org\bovigo\vfs\vfsStreamFile;
use \org\bovigo\vfs\vfsStream;
use warden\IdeaCategory;
use warden\WardenManager;
use warden\Warden;

/**
 * Unit Test for \warden\src\WardenManager
 */
class WardenManagerTest extends TestCase{

    /**
     * @var WardenManager The WardenManager object
     */
    public $wardenManager;

    /**
     * Instantiate a WardenManager and create some mock files
     * into a mock directory to pretend as idea files
     */
    protected function setUp(){
        $this->wardenManager = WardenManager::getWardenManager();
        $this->createMockFileSystem();
    }

    /**
     * Destroy the objects
     */
    protected function tearDown(){
        $this->wardenManager = null;
        vfsStreamWrapper::unregister();
    }
    
    /**
     * Create a mock file system with some mock files to pretend as a directory
     * of JSON's idea files already exported
     */
    public function createMockFileSystem(){
        vfsStreamWrapper::register();
        vfsStreamWrapper::setRoot(new vfsStreamDirectory('mockDir'));
        $mock1 = new vfsStreamFile('mock1.idea');
        $mock1->setContent('{
                            "ID":"abc123def",
                            "Format":"IDEA0",
                            "Attach": [{
                                        "Hash":[ "sha1:794467071687f7c59d033f4de5ece6b46415b633" ],
                                        "Content":"hex456hex",
                                        "Filename":"mock1"
                                      }],
                            "DetectTime":"1996-12-19T16:39:57-08:00",
                            "Category": [
                                        "Abusive.Violence",
                                        "Glorification of violence"
                                        ],
                            "Confidence": 1,
                            "_count": 1
                            }');
        vfsStreamWrapper::getRoot()->addChild($mock1);
        $mock2 = new vfsStreamFile('mock2.idea');
        $mock2->setContent('{
                            "ID":"abc456def",
                            "Format":"IDEA0",
                            "Attach": [{
                                        "Hash":[ "sha1:794467071687f7c59d033f4de5ece6b46415b634" ],
                                        "Content":"hex123hex",
                                        "Filename":"mock2"
                                      }],
                            "DetectTime":"1996-12-19T16:39:57-03:00",
                            "Category": [
                                        "Attempt.NewSignature", 
                                        "An attempt using and unknown exploit."
                                        ],
                            "Confidence": 1,
                            "_count": 1
                            }');
        vfsStreamWrapper::getRoot()->addChild($mock2);
    }
    
    /**
     * Test if the method getIdeasFromFileSystem is reading the filesystem and 
     * returning the readen JSON's into an array like eg: 
     *                        ['filename1'] => [
     *                                              ["id"]  => "123abc",
     *                                              ["name"]=> "idea name",
     *                                              ...
     *                                          ],
     *                        ['filename2] => ...
     * @covers \warden\src\WardenFileSystem::getIdeasFromFileSystem()
     * @return array
     */
    public function testGetWardensFromFileSystem(){        
        $this->wardenManager->setDir(vfsStream::url('mockDir').'/');
        $ideasArray = $this->wardenManager->getIdeasFromFileSystem();
        $this->assertNotEmpty($ideasArray);
        $this->assertArrayHasKey('mock1.idea', $ideasArray);
        $this->assertArrayHasKey('mock2.idea', $ideasArray);
        return $ideasArray;
    }
    
    /**
     * Test if errors on reading directory are treated
     * @covers \warden\src\WardenManager::setDir($dir)
     * @covers \warden\src\WardenManager::getIdeasFromFileSystem()
     * @expectedException Exception
     * @expectedExceptionCode 20
     */
    public function testGetWardensFromFileSystemDirectoryManipulation(){
        $this->wardenManager->setDir('unexistentDir');
        $this->wardenManager->getIdeasFromFileSystem();
    }
    
    /**
     * Test if errors on reading files are treated
     * @covers \warden\src\WardenManager::getIdeasFromFileSystem()
     * @expectedException Exception
     * @expectedExceptionCode 21
     * @todo Incomplete test
     */
    public function testGetWardensFromFileSystemFileManipulation(){
        $this->markTestIncomplete();
        $mock3 = new vfsStreamFile('mock3.invalid');
        $mock3->setContent(null);
        vfsStreamWrapper::getRoot()->addChild($mock3);
        $this->wardenManager->getIdeasFromFileSystem();
    }

    /**
     * Test the import method and make sure that he's 
     * instantiating Warden objects from an array of Json's idea formats
     * @covers \warden\src\WardenFileSystem::import($ideasArray)
     * @covers \warden\src\WardenManager::getWardens()
     * @return array
     * @depends testGetWardensFromFileSystem
     */
    public function testImport($ideasArray){
        $this->wardenManager->import($ideasArray);
        $wardens = $this->wardenManager->getWardens();
        $this->assertInstanceOf(Warden::class, $wardens['mock1.idea']);
        $this->assertInstanceOf(Warden::class, $wardens['mock2.idea']);
        return $wardens;
    }
    
    /**
     * Test exporting the Warden objects to the filesystem
     * @covers \warden\src\WardenManager::export($wardens)
     * @param array $wardens
     * @depends testImport
     */
    public function testExport($wardens){
        $this->wardenManager->setDir(vfsStream::url('mockDir').'/');
        $this->wardenManager->export($wardens);
        $this->assertEquals(
                    'mock1.idea',
                    vfsStreamWrapper::getRoot()
                                    ->getChildren()[0]
                                    ->getName()
                    );
        $this->assertEquals(
                    'mock2.idea',
                    vfsStreamWrapper::getRoot()
                                    ->getChildren()[1]
                                    ->getName()
                    );
    }

    /**
     * Test if the attributes are correct
     * @depends testInstantiateWardensFromArray
     */
    public function testAttributesAreCorrect($wardens){
        $shaMock1 = 'sha1:794467071687f7c59d033f4de5ece6b46415b633';
        $shaMock2 = 'sha1:794467071687f7c59d033f4de5ece6b46415b634';
        $detecTimeMock1 = "1996-12-19T16:39:57-08:00";
        $detecTimeMock2 = "1996-12-19T16:39:57-03:00";
        $this->assertEquals('abc123def',    $wardens['mock1.idea']->getId());
        $this->assertEquals('mock1',        $wardens['mock1.idea']->getName());
        $this->assertEquals($shaMock1,      $wardens['mock1.idea']->getHash());
        $this->assertEquals("hex123hex",    $wardens['mock1.idea']->getHex());
        $this->assertEquals("IDEA0",        $wardens['mock1.idea']->getFormat());
        $this->assertEquals($detecTimeMock1,$wardens['mock1.idea']->getDetectTime());
        $this->assertEquals(IdeaCategory::ABUSIVE_VIOLENCE,
                                            $wardens['mock1.idea']->getCategory());
        $this->assertEquals(1,              $wardens['mock1.idea']->getCount());
        $this->assertEquals('abc456def',    $wardens['mock2.idea']->getId());
        $this->assertEquals('mock2',        $wardens['mock2.idea']->getName());
        $this->assertEquals($shaMock2,      $wardens['mock2.idea']->getHash());
        $this->assertEquals("hex456hex",    $wardens['mock2.idea']->getHex());
        $this->assertEquals("IDEA0",        $wardens['mock2.idea']->getFormat());
        $this->assertEquals($detecTimeMock2,$wardens['mock2.idea']->getDetectTime());
        $this->assertEquals(IdeaCategory::ATTEMPT_NEWSIGNATURE,
                                            $wardens['mock2.idea']->getCategory());
        $this->assertEquals(1,              $wardens['mock2.idea']->getCount());
    }

}