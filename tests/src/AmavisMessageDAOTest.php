<?php

namespace warden\tests\src;

use \PHPUnit\Framework\TestCase;
use warden\AmavisMessageDAO;
use warden\Warden;
use warden\IdeaCategory;

/**
 * Unit Test for \warden\src\AmavisMessageDAO
 */
class AmavisMessageDAOTest extends TestCase {

    /**
     * @var AmavisMessageDAO The Mysql DAO object
     */
    protected $amavisMessageDAO;

    /**
     * @var \PHPUnit\Framework\MockObject
     */
    protected $wardenManager;

    /**
     * Instantiate a AmavisMessage DAO
     */
    protected function setUp() {
        $this->amavisMessageDAO = new AmavisMessageDAO(
                'crate', //database
                'amavis', //table
                IdeaCategory::ABUSIVE_SPAM, //category
                parse_ini_file('databases.ini', true)['crate']['amavis'] //table_fileds=>WardenClassAttributes);
        );
    }

    /**
     * Destroy the Mysql DAO object
     */
    protected function tearDown() {
        $this->amavisMessageDAO = null;
    }

    /**
     * Test if is connected
     * @covers \warden\src\MysqlDAO::isConnected()
     */
    public function testIsConnected() {
        $this->assertTrue($this->amavisMessageDAO->isConnected());
    }

    /**
     * Test if find amavis message by hash works fine
     * @covers AmavisMessageDAO::findByHash($hash)
     */
    public function testGetByHash() {
        $wardens = $this->amavisMessageDAO->findByDetectTime('01:00:00');
        foreach ($wardens as $warden){
            $this->assertInstanceOf(Warden::class, $warden);
        }
    }

}
