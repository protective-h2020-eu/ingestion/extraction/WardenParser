<?php
namespace warden\tests\src;

use \PHPUnit\Framework\TestCase;
use warden\DAOFactory;
use warden\AmavisMessageDAO;

/**
 * Unit Test for \warden\src\DAOFactory
 */
class DAOFactoryTest extends TestCase{

    /**
     * @var DAOFactory The Mysql DAO Factory object
     */
    protected $amavisMessageDAO;

    /**
     * Get a Mysql DAO instance from the Factory
     */
    protected function setUp(){
        $this->amavisMessageDAO = DAOFactory::getDAO(DAOFactory::CRATE_AMAVIS);
    }

    /**
     * Destroy the Mysql DAO object
     */
    protected function tearDown(){
        $this->amavisMessageDAO = null;
    }
    
    /**
     * Guarantee that not implemented DAOS aren't accepted
     * @covers \warden\DAOFactory::getDAO($dao)
     * @expectedException \Exception
     * @expectedExceptionCode 1
     */
    public function testNotAcceptUnimplementedDAOS(){
        $this->markTestIncomplete();
        DAOFactory::getDAO(1112233444);
    }

    /**
     * @covers \warden\DAOFactory::getDAO($dao)
     */
    public function testGetDAO(){
        $this->assertInstanceOf(AmavisMessageDAO::class, $this->amavisMessageDAO);
    }

}